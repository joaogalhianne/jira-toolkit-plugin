package it;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.customfields.CreateCustomFieldPage;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.MatcherAssert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.Matchers.hasItem;

public class TestJiraToolkit extends BaseJiraWebTest
{
    public static final String PLUGIN_KEY = "com.atlassian.jira.toolkit";
    public static final String ENABLED_STATE = "ENABLED";

    static boolean wasToolkitPluginEnabledBeforeTests;

    @BeforeClass
    public static void enableToolkitPluginIfNecessary()
    {
        final String pluginState = jira.backdoor().plugins().getPluginState(PLUGIN_KEY);
        wasToolkitPluginEnabledBeforeTests = pluginState.equals(ENABLED_STATE);
        if (!wasToolkitPluginEnabledBeforeTests)
        {
            jira.backdoor().plugins().enablePlugin(PLUGIN_KEY);
        }
    }

    @AfterClass
    public static void disableToolkitPluginIfNecessary()
    {
        if (!wasToolkitPluginEnabledBeforeTests)
        {
            jira.backdoor().plugins().disablePlugin(PLUGIN_KEY);
        }
    }

    @Category(OnDemandAcceptanceTest.class)
    @LoginAs(admin = true, targetPage = CreateCustomFieldPage.class)
    @Test
    public void testMessageCustomFieldIsVisibleInTheCreateCustomFieldPage()
    {
        CreateCustomFieldPage createCustomFieldPage = pageBinder.bind(CreateCustomFieldPage.class);
        MatcherAssert.assertThat(createCustomFieldPage.getAvailableCustomFields(), hasItem(new CustomFieldItemNameContainsMatcher("Message Custom Field")));
    }

    class CustomFieldItemNameContainsMatcher extends BaseMatcher
    {
        final String prefix;

        CustomFieldItemNameContainsMatcher(final String prefix)
        {
            this.prefix = prefix;
        }

        @Override
        public boolean matches(Object item)
        {
            CreateCustomFieldPage.CustomFieldItem customFieldItem = (CreateCustomFieldPage.CustomFieldItem) (item);
            return (customFieldItem.getName().contains(prefix));
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText("contains \"" + prefix + "\" in the name");
        }
    }

}
