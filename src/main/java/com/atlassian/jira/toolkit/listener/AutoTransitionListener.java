package com.atlassian.jira.toolkit.listener;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.opensymphony.util.TextUtils;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An event listener which triggers a workflow transition under certain conditions. For instance, this can be used to
 * trigger a workflow transition when a user comments or edits an issue in a particular state.
 */
public class AutoTransitionListener extends AbstractIssueEventListener implements IssueEventListener
{
	private final static Logger log = Logger.getLogger(AutoTransitionListener.class);

    // Don't change the text for these parameters, as they are used as parameter keys in the database, and the old
    // parameters will still be present and undeletable.
    protected static final String ACTION = "Action ID";
    protected static final String EVENT_TRIGGERS = "Action that triggered the IssueUpdated event";
    protected static final String EVENT = "Event ID";
    protected static final String STATUS = "Status";
    protected static final String PROJECT = "Project Key";
    protected static final String CONDITION_REPORTER = "Only do when current user is the Reporter?";
    protected static final String CONDITION_ASSIGNEE = "Only do when current user is the Assignee?";
    protected static final String CONDITION_CUSTOMFIELD = "Only do when current user is in (Multi-) User Picker custom field:";
    protected static final String CONDITION_USER = "Only do when current user is the following?";
    protected static final String CONDITION_GROUP = "Only do when current user is in the following groups?";

    private static final String EVENT_TRIGGER_EDIT = "edit";
    private static final String EVENT_TRIGGER_TRANSITION = "transition";
    private static final String EVENT_TRIGGER_ATTACHMENT_ADDED = "attachment_added";
    private static final String EVENT_TRIGGER_ATTACHMENT_DELETED = "attachment_deleted";
	private static final Set<String> POSSIBLE_EVENT_ACTIONS = new HashSet<String>(Arrays.asList(new String[]
			{EVENT_TRIGGER_EDIT, EVENT_TRIGGER_TRANSITION, EVENT_TRIGGER_ATTACHMENT_ADDED, EVENT_TRIGGER_ATTACHMENT_DELETED}));

    private Set<Long> eventIDs = Collections.emptySet();
    private Set<String> statusIDs = Collections.emptySet();
    private Set<String> projectKeys = Collections.emptySet();
    private Set<String> eventTriggers = new HashSet<String>();
    private Set<String> currentIsInCustomField = Collections.emptySet();
    private Set<String> currentIsUser = Collections.emptySet();
    private Set<String> currentIsInGroup = Collections.emptySet();

    private int actionId;
    private boolean allProjects = false;
    private boolean currentIsReporter = false;
    private boolean currentIsAssignee = false;

    @Override
	public String[] getAcceptedParams()
    {
        return new String[]{EVENT, EVENT_TRIGGERS, ACTION, STATUS, PROJECT, CONDITION_REPORTER, CONDITION_CUSTOMFIELD, CONDITION_ASSIGNEE, CONDITION_USER, CONDITION_GROUP};
    }

    @Override
	public String getDescription() {
        return "Transitions an issue given an event to listen for and an action to perform. Fields are as follows:"+
                "<ul style='{ li { margin-left: 10em } }'>"+
                "<li style='margin-top: 1em'><b>"+EVENT+"</b> - comma separated IDs of JIRA Events to listen to. Current possibilities are:<br><select multiple='multiple' size='5' disabled='true'>"+getEventsTable()+"</select>"+
                "<li style='margin-top: 1em'><b>"+EVENT_TRIGGERS+"</b> - Comma separated list of selected triggers (<strong>NB: only applies to IssueUpdated event</strong>). Supported values are <BR><UL>"+getPossibleEventActions()+"</UL><BR> Leave empty for all actions."+
                "<li style='margin-top: 1em'><b>"+ACTION+"</b> - Workflow action to trigger (if the conditions pass). Possible values are:<br><select multiple='multiple' size='15' disabled='true'>"+getActions()+"</select>"+
                "<li style='margin-top: 1em'><b>"+STATUS+"</b> - Only trigger for issues in these statuses (comma separated IDs). Possible values are:<br><select multiple='multiple' size='15' disabled='true'>"+getStatuses()+"</select>"+
                "<li style='margin-top: 1em'><b>"+PROJECT+"</b> - Only trigger for issues in these projects. This is a comma-separated list of keys. Possible keys are:<br><select multiple='multiple' size='3' disabled='true'>"+getProjectKeys()+"</select>"+
                "<li style='margin-top: 1em'><b>"+CONDITION_REPORTER+"</b> - Whether to trigger only when the current user reported the issue (or when other conditions match)"+
                "<li style='margin-top: 1em'><b>"+CONDITION_CUSTOMFIELD+"</b> - Whether to trigger only when the current user is listed in an issue's user-picker custom field (or when other conditions match). The particular custom field to examine is indicated by the ID here. Possible values are:<br>"+"<select multiple='multiple' size='15' disabled='true'>"+getCustomFields()+"</select>"+
                "<li style='margin-top: 1em'><b>"+CONDITION_ASSIGNEE+"</b> - Whether to trigger only when the current user is the issue assignee (or when other conditions match)"+
                "<li style='margin-top: 1em'><b>"+CONDITION_USER+"</b> - Whether to trigger only when the current user is one of the users specified in this field. Take s a comma separated list of userNames."+
                "<li style='margin-top: 1em'><b>"+CONDITION_GROUP+"</b> - Whether to trigger only when the current user is one of the groups specified in this field. Take s a comma separated list of group names."+
                "</ul>";
    }

    @Override
	public void init(Map params)
    {
        if (params.containsKey(ACTION))
        {
            actionId = Integer.parseInt((String) params.get(ACTION));
        }

        eventIDs  = new HashSet<Long>();
        if (params.containsKey(EVENT))
        {
        	String[] tokens = StringUtils.split((String) params.get(EVENT), ", \t;:");
        	for(String token: tokens)
			{
				if(StringUtils.isNumeric(token))
					eventIDs.add(Long.parseLong(token));
			}
        }

        eventTriggers = new HashSet<String>();
        if(params.containsKey(EVENT_TRIGGERS))
        {
        	String[] tokens = StringUtils.split((String) params.get(EVENT_TRIGGERS), ", \t;:");
        	for(String token: tokens)
			{
        		if(StringUtils.isBlank(token) || "all".equalsIgnoreCase(token) || "*".equalsIgnoreCase(token))
        		{
        			eventTriggers.clear();
        			break;
        		}

        		// invalid tokens are ignored
        		if(POSSIBLE_EVENT_ACTIONS.contains(token.toLowerCase()))
        		{
        			eventTriggers.add(token.toLowerCase().trim());
        		}
			}
        }

        statusIDs  = new HashSet<String>();
        if (params.containsKey(STATUS))
        {
        	String[] tokens = StringUtils.split((String) params.get(STATUS), ", \t;:");
        	for(String token: tokens)
        	{
        		if(StringUtils.isNumeric(token))
        			statusIDs.add(token.trim());
        	}
        }

        projectKeys  = new HashSet<String>();
        if (params.containsKey(PROJECT))
        {
            String[] tokens = StringUtils.split(((String) params.get(PROJECT)), ", \t;:");
        	for(String token: tokens)
        	{
    			projectKeys.add(token.toLowerCase().trim());
        	}

        	allProjects = projectKeys.contains("*");
        }

        if (params.containsKey(CONDITION_ASSIGNEE)) {
            currentIsAssignee = (Boolean.valueOf((String) params.get(CONDITION_ASSIGNEE))).booleanValue();
        }
        if (params.containsKey(CONDITION_REPORTER)) {
            currentIsReporter = (Boolean.valueOf((String) params.get(CONDITION_REPORTER))).booleanValue();
        }

        currentIsInCustomField  = new HashSet<String>();
        if (params.containsKey(CONDITION_CUSTOMFIELD))
        {
        	String[] tokens = StringUtils.split((String) params.get(CONDITION_CUSTOMFIELD), ", \t;:");
        	for(String token: tokens)
        	{
    			currentIsInCustomField.add(token.trim().toLowerCase());
        	}
        }

        currentIsUser = new HashSet<String>();
        if (params.containsKey(CONDITION_USER))
        {
        	String[] tokens = StringUtils.split((String) params.get(CONDITION_USER), ", \t;:");
        	for(String token: tokens)
        	{
        		currentIsUser.add(token.trim());
        	}
        }

        currentIsInGroup  = new HashSet<String>();
        if (params.containsKey(CONDITION_GROUP))
        {
        	String[] tokens = StringUtils.split((String) params.get(CONDITION_GROUP), ", \t;:");
        	for(String token: tokens)
        	{
        		currentIsInGroup.add(token.trim().toLowerCase());
        	}
        }

        if (log.isInfoEnabled()) log.info(getInfoMessage());
    }

    private String getInfoMessage()
    {
    	StringBuffer buf = new StringBuffer("AutoTransitionListener configured to transition issues in ");
    	if(allProjects)
    		buf.append("all projects");
    	else if(projectKeys.isEmpty())
			buf.append("no projects (ie. disabled)");
    	else
    		buf.append("projects ");
    	buf.append(projectKeys);
        buf.append(" in status ");
        buf.append(statusIDs);
        buf.append(" on events ");
        buf.append(eventIDs);
        buf.append(" (Updates triggered by ");
        buf.append(eventTriggers);
        buf.append(") via action ");
        buf.append(actionId);
        buf.append(", ");
        buf.append(currentIsAssignee ? "if user is assignee; " : "");
        buf.append(currentIsReporter ? " if user is reporter; " : "");
        buf.append(currentIsInCustomField.isEmpty() ? "":" if user is in customfield "+currentIsInCustomField);
        buf.append(currentIsUser.isEmpty()? "":" if user is in "+ currentIsUser);
        buf.append(currentIsUser.isEmpty()? "":" if user is in "+ currentIsUser);
        buf.append(currentIsInGroup.isEmpty()? "":" if user is in groups "+ currentIsInGroup);
        return buf.toString();
    }

    @Override
	public void workflowEvent(IssueEvent event)
    {
        if(!eventIDs.contains(event.getEventTypeId().longValue()))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is not one of "+eventIDs);
        	return;
        }

        if(!isCorrectEventTrigger(event))
        {
        	if(log.isDebugEnabled())
        		log.debug("Event '"+event.getEventTypeId().longValue()+"': is not triggered by the selected trigger: "+eventTriggers);
        	return;
        }

        Issue issue = event.getIssue();
        if(!isCorrectStatus(issue))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': issue "+issue.getKey()+" in wrong status "+statusIDs+" (current: "+issue.getStatusObject().getId()+")");
        	return;
        }

        if(!isCorrectProject(issue))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': issue "+issue.getKey()+" in wrong project "+projectKeys);
        	return;
        }

        User eventAuthor = getEventAuthor(event);
        if(!isCorrectUser(issue, eventAuthor))
        {
        	if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': user "+eventAuthor.getName()+" is not matched");
        	return;
        }

    	if(log.isDebugEnabled())
    		log.debug("User "+eventAuthor.getName()+" triggered autotransition action "+actionId+" on issue "+issue.getKey());

        // turn on indexing, in case we were triggered from a workflow post-function
        // where indexing would be off. Note that nesting workflow transitions is still
        // not recommended - see http://developer.atlassian.com/jira/browse/JTOOL-28
        boolean wasIndexing = ImportUtils.isIndexIssues();
        ImportUtils.setIndexIssues( true );

        try
		{
            GenericValue issueGV = event.getIssue().getGenericValue();
            final MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue(issueGV);
            executeWorkflowTransition(eventAuthor, issueObject);
		}
		finally
		{
			ImportUtils.setIndexIssues(wasIndexing);
		}
    }

    private void executeWorkflowTransition(final User eventAuthor, final MutableIssue issueObject)
    {
        // Workaround for JRA-40088: WorkflowTransitionUtilImpl sometimes uses user from authentication context
        impersonateUser(new Runnable()
        {
            @Override
            public void run()
            {
                final WorkflowTransitionUtil workflowTransitionUtil = JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
                workflowTransitionUtil.setIssue(issueObject);
                workflowTransitionUtil.setUsername(eventAuthor.getName());
                workflowTransitionUtil.setAction(actionId);

                // JRA-11933 - we want to populate a field values holder with the existing values so we don't
                // overwrite any values in JIRA (summary and issue type). NOTE: we must do this AFTER we
                // have set the issue, user and actionId on the transitionUtil since we need these values to
                // correctly get the FieldScreenRenderer.
                workflowTransitionUtil.setParams(getPopulatedFieldValuesHolder(workflowTransitionUtil, issueObject));

                workflowTransitionUtil.validate();
                workflowTransitionUtil.progress();
            }
        }, eventAuthor);
    }

    private void impersonateUser(final Runnable runnable, final User user)
    {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        final ApplicationUser oldUser = authenticationContext.getUser();
        authenticationContext.setLoggedInUser(user);
        try
        {
            runnable.run();
        }
        finally
        {
            authenticationContext.setLoggedInUser(oldUser);
        }
    }

    private Map getPopulatedFieldValuesHolder(WorkflowTransitionUtil workflowTransitionUtil, Issue issue)
    {
        Map fieldValuesHolder = new HashMap();
        final FieldScreenRenderer fieldScreenRenderer = workflowTransitionUtil.getFieldScreenRenderer();
        for (FieldScreenRenderTab fieldScreenRenderTab : fieldScreenRenderer.getFieldScreenRenderTabs())
        {
        	for (FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem : fieldScreenRenderTab.getFieldScreenRenderLayoutItems())
            {
                if (fieldScreenRenderLayoutItem.isShow(issue))
                {
                    fieldScreenRenderLayoutItem.populateFromIssue(fieldValuesHolder, issue);
                }
            }
        }

        return fieldValuesHolder;
    }

    private boolean isCorrectEventTrigger(IssueEvent event)
	{
    	if(eventTriggers.isEmpty())
    	{
    		if(log.isDebugEnabled())
    			log.debug("EventID '"+event.getEventTypeId().longValue()+"': no event triggers selected.");
    		return true; // all triggers supported
    	}

    	if(event.getEventTypeId() != EventType.ISSUE_UPDATED_ID)
    	{
    		if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is not the IssueUpdated event");

    		return true; // this only applies to IssueUpdated event
    	}

    	if(event.getParams() == null)
    	{
    		if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': has no parameters.");
    		return true; // should never happen
    	}

    	String eventSource = (String) event.getParams().get("eventsource");
    	boolean isTransition = IssueEventSource.WORKFLOW.equalsIgnoreCase(eventSource);
    	if(isTransition && eventTriggers.contains(EVENT_TRIGGER_TRANSITION))
    	{
    		if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is a workflow transition event and transition trigger is selected.");
    		return true; // this is a transition but we do not just bail out in case attachments are mentioned explicitly
    	}

    	boolean isEditSupported = !isTransition && eventTriggers.contains(EVENT_TRIGGER_EDIT); // the issue was edited and edit is a supported trigger

    	GenericValue changeLog = event.getChangeLog();

    	if(changeLog == null)
    	{
    		if(log.isDebugEnabled())
        		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is an action without a change log. Most likely an edit and edit trigger is selected.");
    		return isEditSupported; // the issue was edited
    	}

    	List<GenericValue> changeItems;
		try
		{
			changeItems = changeLog.getRelated("ChildChangeItem");
		}
		catch(GenericEntityException e)
		{
	    	if(log.isDebugEnabled())
	    		log.debug(e);
			return isEditSupported; // should never happen
		}

		if(changeItems == null)
    		return isEditSupported; // nothing has changed - treat it as empty edit

		for(GenericValue changeItem : changeItems)
    	{
    		String fieldName = (String) changeItem.get("field");

    		if("attachment".equalsIgnoreCase(fieldName))
    		{
        		Object newString = changeItem.get("newstring");
        		if(newString == null)
        		{
        			if(log.isDebugEnabled())
        	    		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is an attachment deleted event. Auto transition will "+(eventTriggers.contains(EVENT_TRIGGER_ATTACHMENT_DELETED) ? "" : "NOT ")+"proceed.");
        			return eventTriggers.contains(EVENT_TRIGGER_ATTACHMENT_DELETED);
        		}
        		else
        		{
        			if(log.isDebugEnabled())
        	    		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is an attachment added event. Auto transition will "+(eventTriggers.contains(EVENT_TRIGGER_ATTACHMENT_ADDED) ? "" : "NOT ")+"proceed.");
        			return eventTriggers.contains(EVENT_TRIGGER_ATTACHMENT_ADDED);
        		}
    		}
    	}

		if(log.isDebugEnabled())
    		log.debug("EventID '"+event.getEventTypeId().longValue()+"': is an edit. Auto transition will "+(isEditSupported ? "" : "NOT ")+"proceed.");
		return isEditSupported; // the issue was edited
	}

	/**
     * @return Whether the current user qualifies to trigger this autotransition.
     */
    boolean isCorrectUser(Issue issue, User author)
    {
        ApplicationUser eventAuthor = ApplicationUsers.from(author);
        // if no conditions are set - proceed
    	if(!currentIsAssignee && !currentIsReporter && currentIsInCustomField.isEmpty() && currentIsUser.isEmpty() && currentIsInGroup.isEmpty())
    		return true;

        // If any conditions are set, any one of them evaluating to true will cause us to transition.
    	if(currentIsReporter && issue.getReporter() != null && issue.getReporter().getName().equals(eventAuthor.getName()))
    		return true;
    	else if(log.isDebugEnabled())
    		log.debug("User "+eventAuthor.getName()+" is not the current reporter for issue " + issue.getKey());

        if(currentIsAssignee && issue.getAssignee() != null && issue.getAssignee().getName().equals(eventAuthor.getName()))
        	return true;
        else if(log.isDebugEnabled())
        	log.debug("User "+eventAuthor.getUsername()+" is not the current assignee for issue " + issue.getKey());

        if(currentIsUser.contains(eventAuthor.getKey()))
        	return true;

        else if(log.isDebugEnabled())
    		log.debug("User " + eventAuthor.getUsername() + " is not in "+currentIsUser);

        for(String fieldID: currentIsInCustomField)
        {
        	if(isInCustomField(issue, fieldID, author))
        		return true;
        	else if(log.isDebugEnabled())
        		log.debug("User " + eventAuthor.getUsername() + " is not found in required custom field " + fieldID + " for issue " + issue.getKey());
        }

        GroupManager groupManager = ComponentAccessor.getGroupManager();
        for(String groupName: currentIsInGroup)
		{
			if(groupManager.isUserInGroup(author, groupManager.getGroup(groupName)))
				return true;
		}

        return false;
    }

    /**
     * @param fieldID
     * @return Whether we restrict to users in a userpicker custom field.
     **/
    private boolean isInCustomField(Issue issue, String fieldID, User eventAuthor)
    {
        CustomField cf = getRestrictingCustomField(fieldID);
        if (cf == null)
        {
        	log.debug("Required custom field "+fieldID+" is not found for issue "+issue.getKey());
        	return false;
        }
        Object userObj = issue.getCustomFieldValue(cf);
        if(userObj == null)
        {
        	log.debug("Required custom field "+fieldID+" has no values for issue "+issue.getKey());
        	return false;
        }
        if(userObj instanceof Collection)
        {
        	Collection ccUsers = (Collection) userObj;
	        Iterator iter = ccUsers.iterator();
	        while(iter.hasNext())
	        {
	            Object o = iter.next();
	            if (!(o instanceof User))
	            {
	                log.error("AutoTransitionListener configured to check custom field " + cf + " which is returning object " + o +" of type " + o.getClass() + " rather than a User. Please reconfigure with A userpicker or multi-userpicker custom field.");
	                return false;
	            }

	            User u = (User) o;
	            if (u.getName().equals(eventAuthor.getName()))
	                return true;
	        }
        }
        else if(userObj instanceof User)
        {
        	if(((User)userObj).getName().equals(eventAuthor.getName()))
        		return true;
        }
        else
        {
			log.error("AutoTransitionListener configured to check custom field " + cf + " which is returning unrecognized object type " + userObj.getClass().getName() + " rather than a User or a Collection of Users. Please reconfigure with A userpicker or multi-userpicker custom field.");
        }
        return false;
    }

    /**
     * Parse the user's entry (if any) for a Cc user picker custom field.
     * @param customfieldId eg. "customfield_10000"
     * @return The specified custom field.
     */
    private CustomField getRestrictingCustomField(String customfieldId)
    {
        if (customfieldId == null) return null;
        if ("false".equals(customfieldId)) return null;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        CustomField field = customFieldManager.getCustomFieldObject(customfieldId);
        if (field == null) log.error("AutoTransitionListener configured to restrict transitions to custom field with id '" +
                customfieldId + "', but no such custom field was found. Value expected to be customfield_<id>, eg. customfield_10000.");
        return field;
    }

    private boolean isCorrectStatus(Issue issue)
    {
        return statusIDs.contains(issue.getStatusObject().getId());
    }

    private boolean isCorrectProject(Issue issue) {
        if(allProjects)
        	return true;
        return projectKeys.contains(issue.getProjectObject().getKey().toLowerCase());
    }

    private User getEventAuthor(IssueEvent event)
    {
        User user = event.getUser();
        if (user == null)
            return event.getComment().getAuthorUser();
        return user;
    }


    //whether administrators can delete this listener
    @Override
	public boolean isInternal() {
        return false;
    }

    /**
     * Mail Listeners are unique.  It would be a rare case when you would want two emails sent out.
     */
    @Override
	public boolean isUnique() {
        return false;
    }

    private String getPossibleEventActions()
	{
    	StringBuffer buf = new StringBuffer();
    	for(String action : POSSIBLE_EVENT_ACTIONS)
		{
			buf.append("<li>");
			buf.append(action);
			buf.append("</li>");
		}
		return buf.toString();
	}

	private String getCustomFields()
    {
        StringBuffer buf = new StringBuffer();
        Collection<CustomField> custFields = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects();
        for(CustomField cf: custFields)
        {
            buf.append("<option><b>").append(cf.getId()).append("</b> - ").append(TextUtils.htmlEncode(cf.getName())).append("</option>");
        }
        return buf.toString();
    }

    private String getProjectKeys() {
        StringBuffer buf = new StringBuffer();
        ProjectManager projectManager = ComponentAccessor.getProjectManager();
        Collection<Project> projects = projectManager.getProjectObjects();
        for(Project proj: projects)
        {
            buf.append("<option><b>").append(TextUtils.htmlEncode(proj.getKey())).append("</b></option>");
        }
        return buf.toString();

    }

    private String getEventsTable()
    {
        StringBuffer buf = new StringBuffer();
        Collection eventTypes = ComponentAccessor.getEventTypeManager().getEventTypes();
        Iterator iter = eventTypes.iterator();
        while (iter.hasNext()) {
            EventType e = (EventType) iter.next();
            buf.append("<option><b>").append(e.getId()).append("</b> - ").append(TextUtils.htmlEncode(e.getName())).append("</option>");
        }
        return buf.toString();
    }

    private String getActions()
    {
        StringBuffer buf = new StringBuffer();
        Collection<JiraWorkflow> workflows = ComponentAccessor.getWorkflowManager().getWorkflows();
        for(JiraWorkflow wf: workflows)
        {
            buf.append("<optgroup label='"+ TextUtils.htmlEncode(wf.getName())+"'>");
            buf.append("<ul>");
            for(ActionDescriptor action: wf.getAllActions())
            {
                String srcStep = "";
                if (action.getParent() instanceof StepDescriptor) {
                    StepDescriptor step = (StepDescriptor) action.getParent();
                    srcStep = step.getName();
                }
                int destStepId = action.getUnconditionalResult().getStep();
                String destStep = wf.getDescriptor().getStep(destStepId).getName();
                buf.append("<option><b>").append(action.getId()).append("</b> <i>(").append(TextUtils.htmlEncode(action.getName())).append(")</i>:");

                if(!StringUtils.isBlank(srcStep))
                {
                	buf.append(" From: ");
                	buf.append(TextUtils.htmlEncode(srcStep));
                }

                if(!StringUtils.isBlank(srcStep) && !StringUtils.isBlank(destStep))
                	buf.append(", ");

                if(!StringUtils.isBlank(destStep))
                {
                	buf.append("To: ");
                	buf.append(TextUtils.htmlEncode(destStep));
                }
            }
            buf.append("</optgroup>");
        }
        return buf.toString();
    }

    /**
     * @return A HTML list of options.
     */
    private String getStatuses() {
        StringBuffer buf = new StringBuffer();
        Collection<Status> statuses = ComponentAccessor.getConstantsManager().getStatusObjects();
        for(Status status: statuses)
        {
            buf.append("<option><b>").append(status.getId()).append("</b> - ").append(TextUtils.htmlEncode(status.getName())).append("</option>");
        }
        return buf.toString();
    }

}
