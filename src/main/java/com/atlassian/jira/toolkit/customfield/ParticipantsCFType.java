package com.atlassian.jira.toolkit.customfield;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.comparator.ApplicationUserBestNameComparator;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.AbstractCustomFieldIndexer;
import com.atlassian.jira.notification.type.UserCFNotificationTypeAware;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EasyList;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.util.profiling.UtilTimerStack;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

public class ParticipantsCFType extends CalculatedCFType implements UserCFNotificationTypeAware, UserField
{
    private static final Logger log = Logger.getLogger(ParticipantsCFType.class);

    private final UserConverter userConverter;
    private final JiraAuthenticationContext authenticationContext;
    private final CommentManager commentManager;
    public static final ApplicationUserBestNameComparator NAME_COMPARATOR = new ApplicationUserBestNameComparator();

    public ParticipantsCFType(UserConverter userConverter, JiraAuthenticationContext authenticationContext, CommentManager commentManager)
    {
        this.userConverter = userConverter;
        this.authenticationContext = authenticationContext;
        this.commentManager = commentManager;
    }

    public String getStringFromSingularObject(Object o)
    {
        if (o instanceof ApplicationUser)
        {
            return userConverter.getHttpParameterValue((ApplicationUser) o);
        }
        return userConverter.getString((User) o);
    }

    public ApplicationUser getSingularObjectFromString(String s) throws FieldValidationException
    {
        return userConverter.getUserFromHttpParameterWithValidation(s);
    }

    public Collection<ApplicationUser> getValueFromIssue(CustomField customField, Issue issue)
    {
        UtilTimerStack.push("Getting participants for issue");
        Set<ApplicationUser> participants = new HashSet<ApplicationUser>();

        try
        {
            ApplicationUser assignee = userConverter.getUserFromDbString(issue.getAssigneeId());
            if (assignee != null)
            {
                participants.add(assignee);
            }
        }
        catch (Exception e)
        {
            // Who cares?
            log.warn("Unable to find assignee for issue " + issue.getKey());
        }

        try
        {
            ApplicationUser reporter = userConverter.getUserFromDbString(issue.getReporterId());
            if (reporter != null)
            {
                participants.add(reporter);
            }
        }
        catch (Exception e)
        {
            // Who cares?
            log.warn("Unable to find reporter for issue " + issue.getKey());
        }

        try
        {
            // Get a list of just the Comments visible to the logged in User:
            final List<Comment> visibleComments = commentManager.getCommentsForUser(issue, authenticationContext.getUser());
            for (Comment comment : visibleComments)
            {
                // add comment author to set of participants
                final ApplicationUser author = comment.getAuthorApplicationUser();
                if (author != null && author.getUsername() != null && author.getDisplayName() != null)
                {
                    participants.add(author);
                }
            }
        }
        catch (Exception e)
        {
            log.warn("An error occurred while retrieving comment authors from issue " + issue.getKey());
        }

        List<ApplicationUser> l = new ArrayList<ApplicationUser>(participants);
        Collections.sort(l, NAME_COMPARATOR);
        UtilTimerStack.pop("Getting participants for issue");
        return l;
    }

    public List<FieldIndexer> getRelatedIndexers(final CustomField customField)
    {
        return EasyList.<FieldIndexer>build(new ParticipantsFieldIndexer(ComponentAccessor.getComponent(FieldVisibilityManager.class), customField, userConverter));
    }

    static class ParticipantsFieldIndexer extends AbstractCustomFieldIndexer
    {
        private final CustomField customField;
        private final UserConverter userConverter;

        protected ParticipantsFieldIndexer(final FieldVisibilityManager fieldVisibilityManager, final CustomField customField, final UserConverter userConverter)
        {
            super(fieldVisibilityManager, customField);
            this.customField = customField;
            this.userConverter = userConverter;
        }

        public void addDocumentFieldsSearchable(final Document doc, final Issue issue)
        {
            addDocumentFields(doc, issue, Field.Index.NOT_ANALYZED);
        }

        public void addDocumentFieldsNotSearchable(final Document doc, final Issue issue)
        {
            addDocumentFields(doc, issue, Field.Index.NO);
        }

        public void addDocumentFields(final Document doc, final Issue issue, final Field.Index indexType)
        {
            Collection<ApplicationUser> users = (Collection<ApplicationUser>) customField.getValue(issue);
            if (users != null)
            {
                for (final ApplicationUser user : users)
                {
                    doc.add(new Field(customField.getId(), userConverter.getDbString(user), Field.Store.YES, indexType));
                }
            }
        }

    }
}
