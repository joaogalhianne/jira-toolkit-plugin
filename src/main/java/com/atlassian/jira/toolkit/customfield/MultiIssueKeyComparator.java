package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.comparator.KeyComparator;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.toolkit.RequestAttributeKeys;

import java.util.Comparator;
import java.util.List;

/**
 * This object is <strong>cached</strong> by Lucene. Its input (the list of issue keys) comes from the user.
 * So we either need to ensure that the equals and hashcode method of this object depends on the user input, or
 * we ensure that this object does not hold any state.
 * <p/>
 * At the moment we ensure that this object holds no state.
 */
public class MultiIssueKeyComparator implements Comparator<String>
{
    public int compare(final String o1, final String o2)
    {
        @SuppressWarnings("unchecked")
        final List<String> keys = (List<String>) JiraAuthenticationContextImpl.getRequestCache().get(RequestAttributeKeys.MULTIKEY_SEARCHING);

        if ((keys == null) || (keys.size() == 0))
        {
            return KeyComparator.COMPARATOR.compare(o1, o2);
        }

        for (final String str : keys)
        {
            if (str.equals(o1))
            {
                return -1;
            }
            else if (str.equals(o2))
            {
                return 1;
            }
        }

        return 0;
    }

}